---
title: Kerberos I - Overview
date: 2024-02-20 21:14:14
categories: [Windows, Active Directory]
tags: [Windows,Active Directory]
---

This post, is the first in the series and will aim to provide an overview of the protocol, from its beginnings to the different (ab)use techniques.

![KerberosI](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/kerberos_1-1.png)

*   [Kerberos, again](#kerberosAgain)
*   [Brief History of Kerberos](#history)
*   [Kerberos 101](#kerberos101)
    *   [Kerberos concepts](#concepts)
    *   [Encryption types](#encryption)
    *   [Wireshark & Kerberos decryption](#wireshark)
    *   [Kerberos Authentication Flow](#flow)
        *   [Kerberos Pre Authentication](#preauth)
        *   [AS-REQ (Authentication Service Request)](#as-req)
        *   [AS-REP (Authentication Service Response)](#as-rep)
        *   [TGS-REQ (Ticket-Granting Service Request)](#tgs-req)
        *   [TGS-REP (Ticket-Granting Service Response)](#tgs-rep)
        *   [AP-REQ (Application Server Request)](#AP-REQ)
        *   [AP-REP (Application Server Response)](#AP-REP)
    *   [Public Key Cryptography for Initial Authentication (PKINIT)](#PKINIT)
*   [Resources](#resources)
    
<h2 align="center" id="kerberosAgain">Kerberos, again</h2>
---------------

For many years, the Kerberos protocol has been one of the best friends of offensive security professionals. Leveraging this protocol has brought us many wins during assessments and caused many headaches for ‘blue teamers’, who must always be on point and right every time!

That's why, we've created a series explicitly relating to the Kerberos protocol and a look at the associated headaches.

*   Kerberos I - Kerberos Overview (this post).

The Kerberos Blog series will include and cover:

*   Kerberos II - Credential Access.
*   Kerberos III - User Impersonation.
*   Kerberos IV - Delegations.

With the outline of the series covered, let’s get started …

<h2 align="center" id="history">Brief History of Kerberos</h2>
---------------




The development of the Kerberos protocol began in the **[Athena Project](https://web.mit.edu/krb5/raeburn/kfw/sources/pismere/athena/auth/leash/htmlhelp/html/leash_topic_kerberos_auth_service.htm?ref=labs.lares.com)** at the Massachusetts Institute of Technology (MIT). This project started in 1983 and aimed to provide cutting-edge computing resources, including networked workstations, high-speed networking, and collaborative software tools, to empower students and researchers.

  
Below is an example of the Athena computing environment:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/athena_env.png)

As shown in the diagram above, the environment consisted of a client-server model of distributed computing using a three-tier architecture.  
The environment used, among other features, a name resolution service called [Hesiod](https://debathena.mit.edu/trac/wiki/Hesiod?ref=labs.lares.com), instant messaging through the [Zephyr](https://web.mit.edu/saltzer/www/publications/athenaplan/e.4.1.pdf?ref=labs.lares.com) service, and Kerberos as a system-wide authentication protocol.

The Kerberos v1 was developed for this project to authenticate service requests between two or more trusted hosts across an untrusted network and **provide a Single Sign-On (SSO) authentication** to these services.

*   **Kerberos versions 1** through **3** only operated only within the MIT environment.
*   **Kerberos version 4** was released on **January 24, 1989;** however, it was considered insecure due to several cryptographic issues (_its use of DES encryption_).
*   **Kerberos v5** was released in **1993 and updated in 2005 to solve the problems of the previous versions. This version added additional encryption types, cross-realm authentication, and** the [Generic Security Services Application Program Interface (_GSS-API_)](https://web.mit.edu/kerberos/krb5-1.12/doc/appdev/gssapi.html?ref=labs.lares.com).

Kerberos replaced the Windows New Technology LAN Manager (_NTLM_) as the default AUTHENTICATION protocol from Windows Server 2000 onwards; however, Microsoft still supports NTLM for backward compatibility.

Project Athena Technical Overview video, produced in 1991 with a MIT copyright:

[![MIT - Project Athena](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/kerbi.png)](https://www.youtube.com/watch?v=N7L5RjiCM-c&ab_channel=textfilesdotcom)

<h2 align="center" id="kerberos101">Kerberos 101</h2>
---------------

The Kerberos authentication protocol outlines how clients (**principals**) engage with a network authentication service and provides a mechanism for mutual authentication between entities before establishing a secure network connection.

  
[Security principals](https://learn.microsoft.com/en-us/windows-server/identity/ad-ds/manage/understand-security-principals?ref=labs.lares.com#what-are-security-principals) acquire **tickets** from the [Kerberos Key Distribution Center (_**KDC**_)](https://learn.microsoft.com/en-us/windows/win32/secauthn/key-distribution-center?ref=labs.lares.com) and subsequently present these tickets along with any **authenticators** while establishing a connection. Kerberos tickets serve as the representation of the client's network credentials.  
Here’s a basic representation of the Kerberos Authentication flow:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-140.png)

<h3 align="center" id="kerberos101">Kerberos concepts</h3>
---------------
---------------
Let's walk through some basic concepts in Kerberos:

**Realm:** a domain or administrative boundary that defines a Kerberos authentication space. Realms are often associated with a specific network or organization. e.g.: _lareslabs.local._

**[Principal](https://learn.microsoft.com/en-us/windows-server/identity/ad-ds/manage/understand-security-principals?ref=labs.lares.com)**: unique security entity, such as a user or service identified by a name (**principal name**) within a realm, to which Kerberos can assign a ticket. Principals authenticate themselves to access secured resources using tickets. Some examples of security principals could include a user, a workstation, a server, services (NFS, hosts...).

**Principal Names:** Unique identifier to each entity within the domain in Active Directory. e.g.:_Elliot.a@Lareslabs.local_

**[Service Principal Names (SPNs):](https://learn.microsoft.com/en-us/windows/win32/ad/service-principal-names?ref=labs.lares.com)** unique identifier of a service instance. Kerberos authentication uses SPNs to associate a service instance with a service sign-in account. e.g., _CIFS/fs.Lareslabs.local,_ MSSQLSvc_/SQL01.Lareslabs.local:1433._

SPNs are referenced in the [servicePrincipalName](https://learn.microsoft.com/en-us/windows/win32/adschema/a-serviceprincipalname?ref=labs.lares.com) attribute of the domain machine/user accounts.

**Application/Services Servers (AP):** A host that provides one or more services over the network.

**[Key Distribution Center (KDC)](https://learn.microsoft.com/en-us/windows/win32/secauthn/key-distribution-center?ref=labs.lares.com)**: Implemented as a domain service and located on a domain controller, its primary function is to securely manage the distribution of encryption keys among authorized principals in a network. KDC provides two services that are started automatically by the domain controller's Local Security Authority (LSA) and run as part of the LSA's process:

*   **Authentication Service (AS)**: This service handles the issuing of ticket-granting tickets (TGTs) to connect to the ticket-granting service (TGS) in its domain or other trusted domains. TGTs have an expiration date.
*   **Ticket-Granting Service (TGS)**: issues service tickets, allowing authenticated users to access specific network services. Upon presenting a Ticket-Granting Ticket (TGT), the TGS verifies and provides a time-limited service ticket for the requested service.

**Tickets:** Kerberos tickets are encrypted structures that ensure confidentiality and integrity during transmission. This encryption helps protect sensitive information, and only entities with the appropriate keys (_such as the user, TGS, and target service_) can decrypt and verify the contents of the tickets.

Kerberos tickets are encrypted **using the Kerberos keys derived from the user’s password** and additional data.

_Note_: We will discuss tickets in-depth in the third part of the series, _**Kerberos III— User Impersonation.**_

Tickets have an expiration time, although they may be renewable and reusable.

*   **Ticket-granting tickets (TGT):** cryptographic structures obtained by a user during the initial authentication process. They are encrypted using the user's long-term secret key and can be used to request service tickets without requiring the user to re-enter their credentials (_allow Single Sign-On_).
*   **Service Ticket (ST)**: obtained from the TGS after presenting a valid TGT and specifying the desired service to access. Contains information about the user, the service, and a session key for secure communication.

**Authenticators**: Data structures created and encrypted by the client with the session keys provided by the Authentication Services ( KDC).

**[Privilege Attribute Certificate (PAC):](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-pac/166d8064-c863-41e1-9c23-edaaa5f36962?ref=labs.lares.com)** Kerberos is an **authentication** protocol, not an authorization protocol, this is why this data structure was created, to provide this authorization data for Kerberos extensions.

The PAC is included in nearly every ticket (Authorization Data) that encodes **authorization** information, consisting of group memberships, additional credential information, profile and policy information, and supporting security metadata.

Below is an example of the information that can be found within the PAC, obtained through a network traffic capture:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-105.png)

When using Kerberos service tickets for server authentication, the Privilege Attribute Certificate (PAC) can be extracted from a user's ticket. This provides the server with information about user privileges without the need to query the domain controller.


<h3 align="center" id="encryption">Encryption Types</h3>
---------------
---------------

As defined in [\[](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-kile/2a32282e-dd48-4ad9-a542-609804b02cc9?ref=labs.lares.com)**_[MS-KILE](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-kile/2a32282e-dd48-4ad9-a542-609804b02cc9?ref=labs.lares.com)_**[\]](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-kile/2a32282e-dd48-4ad9-a542-609804b02cc9?ref=labs.lares.com), The Kerberos V5 protocol supports multiple encryption types, which are the **algorithms for encrypting the tickets or other data**. The Kerberos V5 protocol negotiates which encryption type to use for a particular connection.

Windows **must** support :

*   AES256-CTS-HMAC-SHA1-96 \[18\]
*   AES128-CTS-HMAC-SHA1-96 \[17\]

And **should** support:

*   RC4-HMAC \[23\]
*   DES-CBC-MD5 \[3\]
*   DES-CBC-CRC \[1\]

The following example shows how to enable the encryption types for a machine account (FS1$) through the [msDS-SupportedEncryptionTypes](https://techcommunity.microsoft.com/t5/core-infrastructure-and-security/decrypting-the-selection-of-supported-kerberos-encryption-types/ba-p/1628797?ref=labs.lares.com) attribute

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-116.png)
More information on how to set encryption types in Kerberos can be found in the [**Microsoft's documentation**.](https://learn.microsoft.com/es-es/archive/blogs/openspecification/windows-configurations-for-kerberos-supported-encryption-type?ref=labs.lares.com)

<h3 align="center" id="wireshark">Wireshark & Kerberos decryption</h3>
---------------
---------------


To better understand what happens within the Kerberos authentication flow, it is possible to provide Wireshark with a key tab file containing Kerberos principal's encryption keys.

To facilitate this, we will need the Kerberos keys of the principals participating in the communication, for example, krbtgt, Elliot.A, and FS1$. To obtain these keys, we can use a number of common tools, such as mimikatz, secretsdump, etc.

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-16.png)

The next step is to add the keys to the [keytab.py script](https://github.com/dirkjanm/forest-trust-tools/blob/master/keytab.py?ref=labs.lares.com) provided by [@dirkjanm](https://twitter.com/_dirkjan?ref=labs.lares.com):

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-17.png)

Import the output file from keytab.py to Wireshark (Edit>Preferences>Protocols>Krb5):

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-22.png)

After that, within the Wireshark capture, we should be able to see several things:

•  **Blue highlighted parts,** Wireshark has been able to decrypted information using the Kerberos keys of the different principals.  
• **Yellow highlighted parts,** indications of missing key data, resulting the blob not being decrypted.

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-47.png)
<h3 align="center" id="flow">Kerberos Authentication Flow</h3>
---------------
---------------

The image below shows the Kerberos authentication flow, using our example of a client (Elliot.A) requesting access to a shared folder (CIFS service) on the server FS1.Lareslabs.local:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-121.png)
In Wireshark, the traffic that is generated when a domain user requests a network resource:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-56.png)
<h4 align="center" id="preauth"> Kerberos Pre-Authentication</h4>
---------------
---------------

Kerberos supports different types of pre-authentication including password-based and public key cryptography [PKINIT](#PKINIT). The most common is using a Timestamp, (PA-ENC-TIMESTAMP). This is just the current timestamp encrypted with the client's key.

Pre-authentication in the Kerberos protocol involves the KRB\_AS\_REQ (Authentication Service Request) and KRB\_AS\_REP (Authentication Service Reply) messages.

This pre-authentication process is enabled in all accounts by default, but it is possible to disable it with the flag "Do not require pre-authentication":

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-129.png)
The client will first send the AS-REQ without any pre-authentication data. If the server requires pre-authentication for this principal (**default option)** it will return an error (KRB5KDC\_ERR\_PREAUTH\_REQUIRED) to the client to indicate that pre-authentication is expected.

If you find an account with this option enabled, you can obtain an AS-REP message without knowing the client's secret key for that account (ASREProasting). We will discuss this and other ‘Roasting’ techniques in the second part of the series: Kerberos II—_Credential Access._

In the following scenario, Elliot.A., a domain user of the Lareslabs.local domain, wants to start the negotiation process with Kerberos. First, they send an AS-REQ message with his username (cname) to the authentication server (KDC):

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-108.png)
By default, all domain accounts have pre-authentication enabled; as such the KDC will return an error (KRB5KDC\_ERR\_PREAUTH\_REQUIRED):

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-109.png)

The following image illustrates an initial AS-REQ request from the client without pre-authentication data, along with the response from the KDC:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-118.png)
It is possible to use these error messages to confirm that a user account exists. We will cover this technique in depth in the post: **_Kerberos II - Credential Access_**.

Once the KDC confirms that the client requires pre-authentication, the client must send a second AS-REQ message with more data to authenticate against the KDC.
<h4 align="center" id="as-req">AS-REQ (Authentication Service Request</h4>
---------------
---------------

The client requests a TGT from the authentication service (AS) at this stage. To do this, the client must send an AS-REQ message.

This message will include, but are not limited to, the following data:

*   **Principal Name** (cname):
*   **Pre-authentication information** (PA-ENC-TIMESTAMP): Timestamp encrypted with the client's key).

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-107.png)
It is essential to know that the encrypted timestamp is only necessary if the  user requires pre-authentication, as we have already discussed, is the **default option** unless the user has the DONT\_REQ\_PREAUTH flag enabled.

The following image below depicts an example of the AS-REQ message captured using Wireshark:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-63.png)
In the message body, it is possible to find in the padata field, the TimeStamp encrypted with the client's secret key (pA-ENC-TIMESTAMP).
<h4 align="center" id="as-rep">AS-REP (Authentication Service Response)</h4>
---------------
---------------
The KDC attempts to decrypt the Timestamp sent by the client in the previous AS-REQ message using the client's secret key. In case of pre-authentication failure, e.g. if the client does not provide a correct password, an error message KDC\_ERR\_PREAUTH\_FAILED will be returned.

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-124.png)
This message contains, among other data:

*   **Ticket Granting Ticket (TGT)**, includes the session key encrypted with the ticket granting service secret key (krbtgt secret key), user name, expiration date, and the [PAC](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-pac/54d570b1-fc54-4c54-8218-f770440ec334?ref=labs.lares.com) (provide SIDs, RIDs, user information, password credentials, used for smart card authentication, S4U data..).
*   **Encrypted data:** The session key is encrypted with the user´s secret key.
![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-9.png)
<h4 align="center" id="tgs-req">TGS-REQ (Ticket-Granting Service Request)</h4>
---------------
---------------

The client sends a TGS-REQ message requesting a service ticket (ST) to access a specific service. The client includes, along with other data:

*   **Ticket-Granting Ticket (TGT)** with an "enc-part" encrypted with the krbtgt Kerberos key. It also includes the PAC.
*   **Authenticator:** Encrypted with the user's login session key.
*   **Service Principal Name (SPN)** of the requested service.

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-59.png)


The Wireshark capture below identifies that we have found a TGS\_REQ message. In the PA-TGS-REQ block, we can see the Ticket Granting Ticket (TGT) and the authenticator sent by the client (Elliot.A). Additionally, in the ‘re-body’ blob, we can identify the service name for which access is being requested (CIFS/FS1):

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-58.png)
<h4 align="center" id="tgs-rep">TGS-REP (Ticket-Granting Service Response)</h4>
---------------
---------------

After receiving the TGS-REQ message and verifying the validity of its Ticket-Granting Ticket (TGT), the KDC returns a response through a TGS\_REP message with:

*   **Service Ticket (TGS)**, encrypted with the service owner (FS1$) Kerberos key.
*   **Encrypted data (EncTGSRepPart)** is encrypted with a copy of the service session key and the user logon session key.

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-126.png)
The image below depicts a TGS-REP message, along with key points to note:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-11.png)
<h4 align="center" id="ap-req">AP-REQ (Application Server Request)</h4>
---------------
---------------
The client forwards a service request message to the intended service, with the following data, among other information:

*   **Service Ticket (ST):** Encrypted with the server kerberos key (FS1$).
*   **Authenticator**: Timestamp encrypted with the current service key.

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-128.png)
Here is a snippet of the related network traffic:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-13.png)
If the PAC validation is enabled, which isn't by default, the AP will verify the PAC against the KDC. Also, if mutual authentication is needed, it will respond to the user with a KRB\_AP\_REP message.

**Optional - [KERB\_VERIFY\_PAC](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-apds/1d1f2b0c-8e8a-4d2a-8665-508d04976f84?ref=labs.lares.com) \[[Need to enable Kerberos PAC validation](https://learn.microsoft.com/es-es/archive/blogs/openspecification/understanding-microsoft-kerberos-pac-validation?ref=labs.lares.com)\]**:

[PAC validation](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-apds/1d1f2b0c-8e8a-4d2a-8665-508d04976f84?ref=labs.lares.com) is the procedure of confirming the authenticity of a user's PAC during authentication with a server. The AP will verify the PAC included in the TGS against the KDC. If mutual authentication is required, it will respond with an AP\_REP message to the user.

This PAC validation can be enabled in the registry key ValidateKdcPacSignature (“ValidateKdcPacSignature”=dword:00000001), in the path:

\[HKEY\_LOCAL\_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Lsa\\Kerberos\\Parameters\].

By default, the value of this entry is 0, so Kerberos does not perform PAC validation on a process that runs as a service.

**Optional - RPC status code (PAC Verify Response)**: The domain controller verifies the signature on the response and returns the result to the server. When the method completes, the server operating system MUST examine the return code to determine if the PAC contents have been altered. The server operating system MUST treat any nonzero return code as a failure.
<h4 align="center" id="ap-rep">AP-REP (Application Server Response)</h4>
---------------
---------------
If mutual authentication is required in the AP\_REQ request ([ISC\_REQ\_MUTUAL\_AUTH](https://googleprojectzero.blogspot.com/2021/10/using-kerberos-for-authentication-relay.html?ref=labs.lares.com) request flag), the server must respond with a timestamp encrypted with its own session key:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-53.png)

Once the client receives it and can decrypt it correctly, it will verify that the server is legitimate. If it does not receive a valid AP\_REP, the client can confirm that it is a fake/rogue server and stop the communication.
<h4 align="center" id="PKINIT">Public Key Cryptography for Initial Authentication in Kerberos (PKINIT)</h4>
---------------
---------------
As described in the [MIT documentation, PKINIT](https://web.mit.edu/kerberos/krb5-1.13/doc/admin/pkinit.html?ref=labs.lares.com) is a pre-authentication mechanism for Kerberos 5 that uses X.509 certificates to authenticate the KDC to clients and vice versa. PKINIT can also enable anonymity support, allowing clients to communicate securely with the KDC or with application servers without authenticating as a particular client principal.

PKINIT makes use of the following new pre-authentication types: PA\_PK\_AS\_REQ and PA\_PK\_AS\_REP.

In the context of using PKINIT, the communication flow would be as follows:

The client (Elliot.A) uses its X.509 certificate (signed by the Certificate Authority) and an authenticator (timestamp encrypted with the client´s private key):

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-90.png)
Below is the PK-AS-REQ request in Wireshark, including the certificate and timestamp sent by the client:

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-85.png)
The authentication server (AS) verifies the certificate, signature and client binding.

If everything is OK, the KDC responds with an TGT encrypted with the krbtgt´s private key and a session key.

In addition, **KDC** i**ncludes the user's LM and NT hash in the PAC (PAC\_CREDENTIAL\_INFO structure) of the TGT.** This feature will allow the client to switch to NTLM if the remote host does not accept Kerberos.

Since the PAC is embedded within the TGT, which is encrypted with one of the Kerberos keys for the krbtgt account, extracting the PAC is currently impossible.

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-46.png)

![](https://gitlab.com/RayRT/rayrt.gitlab.io/-/raw/main/assets/image-86.png)
Due to this, it is possible to use the User to User (U2U) extension to request a TGS ticket with PAC encrypted with the TGT´s session key instead of the krbtgt key and get the user's hash.

In the next post of the series, we will delve deeper into the UnPAC the Hash technique to explore this process more comprehensively.

Wrapping things up ...

In the first part of the Kerberos series, we’ve set the groundwork for the following parts of the series, covering the following:

*   An overview of Kerberos.
*   The concepts.
*   Encryption types.
*   The Kerberos Authentication Flow.
*   Public Key Cryptography for Initial Authentication (PKINIT).

This concludes our first approach to Kerberos. In the next post, we will explore different (ab)use techniques and discuss ticket and delegation management in depth. The following posts will be published in the future, and their posts can be found linked below.

*   Kerberos II - Credential Access
*   Kerberos III - User Impersonation.
*   Kerberos IV - Delegations.

<h2 align="center" id="resources">Resources</h2>
---------------


*   Attl4s - [You do (not) Understand Kerberos](https://attl4s.github.io/?ref=labs.lares.com).
*   [Looking back at Project Athena](https://news.mit.edu/2018/mit-looking-back-project-athena-distributed-computing-for-students-1111?ref=labs.lares.com).
*   Microsoft - [Kerberos Authentication Overview](https://learn.microsoft.com/en-us/windows-server/security/kerberos/kerberos-authentication-overview?ref=labs.lares.com).
*   MIT - [Kerberos papers and documentation](https://web.mit.edu/kerberos/papers.html?ref=labs.lares.com#krb4).
*   GoogleProjectZero - [Kerberos Relay Requirements](https://googleprojectzero.blogspot.com/2021/10/using-kerberos-for-authentication-relay.html?ref=labs.lares.com).
*   Maksim Chudakov - [I understand](https://www.chudamax.com/?ref=labs.lares.com).
*   [Kerberos PKINIT: what, why, and how (to break it)](https://archive.fosdem.org/2023/schedule/event/security_kerberos_pkinit/?ref=labs.lares.com).
*   Tarlogic - [Kerberos](https://www.tarlogic.com/blog/how-kerberos-works/?ref=labs.lares.com)

