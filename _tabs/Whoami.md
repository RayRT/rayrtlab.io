---
title: Whoami
icon: fas fa-info-circle
order: 2
---

Hey folks!,

During my infosec journey, I have had the pleasure of meeting great friends and colleagues from whom I have learned a lot. In addition to great infosec researchers who have made me improve everyday. Recently I decided that I wanted to contribute to the community. I just want to to keep learning, researching and if along the way what you find here results to be useful, I will have achieved the goal.

About me? I’ve been working as a pentester for about 6 years, I started doing web/mobile assessments as most people do in this sector. Step by step I managed to enroll in more and more interesting projects, being able to work in all kind of areas: WiFi, Internal Networks, Social Engineering campaigns, Cloud Pentesting as well as Red Team and Purple Team/Adversary Emulation projects.

Currently, I also serve as an associate teacher in the Master's Degree program in Cybersecurity and Information Security at Castilla-La-Mancha university.

Since some time ago I wanted to focus my skills in diving deeper into the internal networks field. I love to spend the time researching about Windows internals and Active Directory, but I also try to keep my knowledge up to date regarding cloud pentesting (Azure and AWS).

Although certifications are the least important thing, I have tried to enroll in those that I considered to be more useful in terms of knowledge and skills to learn along my career, so I did: OSEP, OSCP, OSWP, OSWE, CRTE, CRTO, labs from HTB like Cybernetics and APTLabs, and cloud trainings like Azure fundamentals and AzRTP and paCSP from Pentester Academy. Currently, I am about to start the RTO2, so I will use this blog to write what I learn during the course.

See you soon!
