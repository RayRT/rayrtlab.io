---
title: Slides
icon: fas fa-archive
order: 1
---

The best way to learn for me, is to create small talks/slides/posts about what I want to investigate. Here I will post the material I did on this journey. Of course everything I have learned here, someone has discovered it before, SO --> **thanks to the researchers and colleagues of infosec for the great work that helps me every day to improve my pentesting skills**, in each post/slides you will find the references on which I have based.


---

### AWS pentest, Identity and Access Management (IAM)

[**Cloud - AWS Pentest IAM**](https://github.com/RayRRT/AWS-Identity-and-Access-Management-IAM-pentest-slides/raw/main/Cloud%20-AWS%20Pentest%20IAM.pptx)

---

---

### Active Directory Certificate Services abuse

[**Active Directory Certificate Services Abuse**](https://github.com/RayRRT/ADCS/blob/main/Active%20Directory%20Certificate%20Abuse.pptx?raw=true)

---


---

### Shadow Credentials

[**AD Persistence methods - Shadow Credentials**](https://github.com/RayRRT/AD_Persistence-Shadow-Credentials-slides/raw/main/AD_Persistence%20-%20ShadowCredentials.pptx)

---


---

### Android Basics I

[**Android - Pentesting Basics I**](https://github.com/RayRRT/Android-Pentesting/raw/main/Android%20Pentesting%20Basics.pptx)

---
